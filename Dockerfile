FROM adoptopenjdk/openjdk11:latest
COPY target/microservice-city.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]