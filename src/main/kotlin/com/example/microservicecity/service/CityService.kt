package com.example.microservicecity.service

import com.example.microservicecity.entity.City
import com.example.microservicecity.repository.CityRepository
import org.springframework.stereotype.Service

@Service
class CityService(val cityRepository: CityRepository) {

    fun listAll() = cityRepository.findAll()

    fun get(id: Long) = cityRepository.findById(id)

    fun add(city: City) = cityRepository.save(city)
}