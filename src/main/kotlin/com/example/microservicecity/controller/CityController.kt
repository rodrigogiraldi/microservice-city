package com.example.microservicecity.controller

import com.example.microservicecity.entity.City
import com.example.microservicecity.service.CityService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/city")
class CityController(val cityService: CityService) {

    @GetMapping("/")
    fun listAll() = cityService.listAll()

    @PostMapping("/")
    fun add(@RequestBody city: City) = cityService.add(city)

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = cityService.get(id)
}