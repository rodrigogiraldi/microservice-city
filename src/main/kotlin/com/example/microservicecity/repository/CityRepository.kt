package com.example.microservicecity.repository

import com.example.microservicecity.entity.City
import org.springframework.data.jpa.repository.JpaRepository

interface CityRepository : JpaRepository<City, Long> {
}