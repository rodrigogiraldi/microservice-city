package com.example.microservicecity.entity

import javax.persistence.*

@Entity
@Table
class City {
    @Id
    @GeneratedValue
    @Column
    var id: Long = 0

    @Column
    var name: String = ""
}