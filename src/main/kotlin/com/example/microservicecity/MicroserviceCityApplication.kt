package com.example.microservicecity

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MicroserviceCityApplication

fun main(args: Array<String>) {
	runApplication<MicroserviceCityApplication>(*args)
}
